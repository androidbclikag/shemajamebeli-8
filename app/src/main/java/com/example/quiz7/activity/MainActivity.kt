package com.example.quiz7.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.quiz7.models.InfoModel
import com.example.quiz7.R
import com.example.quiz7.adapters.CardsRecyclerViewAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.cards_main_layout.*

class MainActivity : AppCompatActivity() {

    private var cardItems = listOf<Array<InfoModel>>()
    private lateinit var adapter: CardsRecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cards_main_layout)
        getJson()    }

    private fun init() {

        cardsRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = CardsRecyclerViewAdapter(cardItems)
        cardsRecyclerView.adapter = adapter

    }


    private fun getJson() {
        cardItems = Gson().fromJson(json, Array<Array<InfoModel>>::class.java).toList()
        init()
        d("log", cardItems.toString())

//        (cardItems.indices).forEach {
//            d("log", cardItems[it].toString())
//            (cardItems[it].indices).forEach {
//                val model = InfoModel()
//                val item = cardItems[it][it]
//                d("logcard", item.toString())
//                model.field_id = item.field_id
//                model.field_type = item.field_type
//                model.hint = item.hint
//                model.keyboard = item.keyboard
//                model.required = item.required
//                model.is_active = item.is_active
//                model.icon = item.icon
//            }
//        }


//        val local_json = JSONArray(json)
//        (0 until local_json.length()).forEach {
//            val arrayItem = local_json[it] as JSONArray
//            (0 until arrayItem.length()).forEach {
//                d("item", arrayItem[it].toString())
//                val model = InfoModel()
//                val item = arrayItem[it] as JSONObject
//                if (item.has("field_id"))
//                    model.field_id = item.getInt("field_id")
//                if (item.has("hint"))
//                    model.hint = item.getString("hint")
//                if (item.has("field_type"))
//                    model.field_type = item.getString("field_type")
//                if (item.has("keyboard"))
//                    model.keyboard = item.getString("keyboard")
//                if (item.has("required"))
//                    model.required = item.getBoolean("required")
//                if (item.has("is_active"))
//                    model.is_active = item.getBoolean("is_active")
//                if (item.has("icon"))
//                    model.icon = item.getString("icon")
//                    cardItems = model
//                    d("field_id", item.getInt("field_id").toString())


            }






    private val json = "[\n" +
            "   [\n" +
            "      {\n" +
            "         \"field_id\":1,\n" +
            "         \"hint\":\"UserName\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"required\":false,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":2,\n" +
            "         \"hint\":\"Email\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"required\":true,\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":3,\n" +
            "         \"hint\":\"phone\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"required\":true,\n" +
            "         \"keyboard\":\"number\",\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"\n" +
            "      }\n" +
            "   ],\n" +
            "   [\n" +
            "      {\n" +
            "         \"field_id\":4,\n" +
            "         \"hint\":\"Full Name\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"required\":true,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":14,\n" +
            "         \"hint\":\"Jemali\",\n" +
            "         \"field_type\":\"input\",\n" +
            "         \"keyboard\":\"text\",\n" +
            "         \"required\":false,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://jemala.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":89,\n" +
            "         \"hint\":\"Birthday\",\n" +
            "         \"field_type\":\"chooser\",\n" +
            "         \"required\":false,\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://i.ibb.co/S0FnzrQ/birthday.png\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"field_id\":898,\n" +
            "         \"hint\":\"Gender\",\n" +
            "         \"field_type\":\"chooser\",\n" +
            "         \"required\":\"false\",\n" +
            "         \"is_active\":true,\n" +
            "         \"icon\":\"https://i.ibb.co/t2C06j7/gender-fluid.png\"\n" +
            "      }\n" +
            "   ]\n" +
            "]\n" +
            "\n"



    fun register(view: View) {
        Toast.makeText(this, "this task is not completed yet", Toast.LENGTH_SHORT).show()


    }


}




