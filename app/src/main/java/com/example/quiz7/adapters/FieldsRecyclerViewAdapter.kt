package com.example.quiz7.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz7.models.InfoModel
import com.example.quiz7.R
import com.example.quiz7.databinding.ChooserItemLayoutBinding
import com.example.quiz7.databinding.InputItemLayoutBinding

class FieldsRecyclerViewAdapter(private val items: Array<InfoModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val CHOOSER_VIEW_HOLDER = 2
        const val INPUT_VIEW_HOLDER = 1
    }

    inner class ChooserViewHolder(private val binding: ChooserItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val model = items[adapterPosition]
            binding.chooser = model

        }

    }

    inner class InputViewHolder(private val binding: InputItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            val model = items[adapterPosition]
            binding.input = model

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == CHOOSER_VIEW_HOLDER)
            ChooserViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.chooser_item_layout,
                    parent,
                    false
                )
            )
        else
            InputViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.input_item_layout,
                    parent,
                    false
                )
            )

    }

    override fun getItemCount(): Int = items.size


    override fun getItemViewType(position: Int): Int {
        val model = items[position]
        return if (model.field_type == "chooser")
            CHOOSER_VIEW_HOLDER
        else
            INPUT_VIEW_HOLDER

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ChooserViewHolder -> holder.onBind()
            is InputViewHolder -> holder.onBind()
        }

    }
}