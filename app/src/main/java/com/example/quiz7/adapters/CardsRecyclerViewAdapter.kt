package com.example.quiz7.adapters

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.quiz7.models.InfoModel
import com.example.quiz7.R
import kotlinx.android.synthetic.main.card_item_layout.view.*

class CardsRecyclerViewAdapter(private val childRecyclerView: List<Array<InfoModel>>) :
    RecyclerView.Adapter<CardsRecyclerViewAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var adapter: FieldsRecyclerViewAdapter
        fun onBind() {
            val itemModel = childRecyclerView[adapterPosition]
            d("position", childRecyclerView[adapterPosition].size.toString())
            itemView.fieldsRecyclerView.layoutManager = LinearLayoutManager(itemView.context)
            adapter =
                FieldsRecyclerViewAdapter(itemModel)
            itemView.fieldsRecyclerView.adapter = adapter

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.card_item_layout, parent, false)
    )

    override fun getItemCount(): Int = childRecyclerView.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


}