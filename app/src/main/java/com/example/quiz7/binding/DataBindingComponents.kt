package com.example.quiz7.binding

import android.graphics.drawable.Drawable
import android.text.InputType.TYPE_CLASS_NUMBER
import android.text.InputType.TYPE_CLASS_TEXT
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.EditText
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

object DataBindingComponents {

    @JvmStatic
    @BindingAdapter("setInputType")
    fun setInputType(editText: EditText, keyBoard: String) {
        if (keyBoard == "text")
            editText.inputType = TYPE_CLASS_TEXT
        else
            editText.inputType = TYPE_CLASS_NUMBER

    }


    @JvmStatic
    @BindingAdapter("setDrawableEditText")
    fun setDrawableEditText(editText: EditText, drawableUrl: String) {
        if (drawableUrl.isNotEmpty()) {
            Glide.with(editText.context).asDrawable().load(drawableUrl)
                .into(object : CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        editText.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            resource,
                            null
                        )

                    }

                }

                )
        }

    }

    @JvmStatic
    @BindingAdapter("setDrawableButton")
    fun setDrawableButton(button: Button, drawableUrl: String) {
        if (drawableUrl.isNotEmpty()) {
            Glide.with(button.context).asDrawable().load(drawableUrl)
                .into(object : CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) {

                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        button.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            null,
                            null,
                            resource,
                            null
                        )

                    }

                }

                )
        }

    }

    @JvmStatic
    @BindingAdapter("setVisibility")
    fun View.setVisibility(visible: Boolean) {
        visibility = if (visible)
            VISIBLE
        else
            GONE

    }



}
